export const Username = `john_doe@gmail.com`;

export const MainTitle = `312 places to stay in Amsterdam`;

export const Sort = [
  {
    label: `Popular`,
    value: `Popular`,
  },
  {
    label: `Price: low to high`,
    value: `asc_price`,
  },
  {
    label: `Price: high to low`,
    value: `desc_price`,
  },
  {
    label: `Top rated first`,
    value: `top`,
  },
];

export const OfferCards = [
  {
    id: `1`,
    mark: `Premium`,
    href: `#`,
    img: `img/apartment-01.jpg`,
    price: `€120`,
    attribute: `night`,
    hasBookmark: false,
    rating: 4,
    name: `Beautiful & luxurious apartment at great location`,
    type: `Apartment`,
  },
  {
    id: `2`,
    href: `#`,
    img: `img/room.jpg`,
    price: `€80`,
    attribute: `night`,
    hasBookmark: true,
    rating: 4,
    name: `Wood and stone place`,
    type: `Private room`,
  },
  {
    id: `3`,
    href: `#`,
    img: `img/apartment-02.jpg`,
    price: `€132`,
    attribute: `night`,
    hasBookmark: false,
    rating: 4,
    name: `Canal View Prinsengracht`,
    type: `Apartment`,
  },
  {
    id: `4`,
    mark: `Premium`,
    href: `#`,
    img: `img/apartment-03.jpg`,
    price: `€180`,
    attribute: `night`,
    hasBookmark: false,
    rating: 5,
    name: `Nice, cozy, warm big bed apartment`,
    type: `Apartment`,
  },
  {
    id: `5`,
    href: `#`,
    img: `img/room.jpg`,
    price: `€80`,
    attribute: `night`,
    hasBookmark: false,
    rating: 4,
    name: `Nice, cozy, warm big bed apartment`,
    type: `Private room`,
  },
];

export const FavoritesList = [
  {
    city: `Amsterdam`,
    id: `11`,
    items: [
      {
        id: `1`,
        mark: `Premium`,
        href: `#`,
        img: `img/apartment-01.jpg`,
        price: `€120`,
        attribute: `night`,
        hasBookmark: true,
        rating: 4,
        name: `Beautiful & luxurious apartment at great location`,
        type: `Apartment`,
      },
      {
        id: `2`,
        href: `#`,
        img: `img/room.jpg`,
        price: `€80`,
        attribute: `night`,
        hasBookmark: true,
        rating: 4,
        name: `Wood and stone place`,
        type: `Private room`,
      },
    ],
  },
  {
    city: `Cologne`,
    id: `22`,
    items: [
      {
        id: `3`,
        href: `#`,
        img: `img/apartment-02.jpg`,
        price: `€132`,
        attribute: `night`,
        hasBookmark: true,
        rating: 4,
        name: `Canal View Prinsengracht`,
        type: `Apartment`,
      },
    ]
  },
];

export const CitiesList = [
  {
    id: `111`,
    name: `Paris`,
  },
  {
    id: `222`,
    name: `Cologne`,
  },
  {
    id: `333`,
    name: `Brussels`,
  },
  {
    id: `444`,
    name: `Amsterdam`,
  },
  {
    id: `555`,
    name: `Hamburg`,
  },
  {
    id: `666`,
    name: `Dusseldorf`,
  },
];
